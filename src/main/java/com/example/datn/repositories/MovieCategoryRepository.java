package com.example.datn.repositories;

import com.example.datn.entity.MovieCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovieCategoryRepository extends JpaRepository<MovieCategory, Integer> {

    List<MovieCategory> findAllByMovieId(Integer movieId);

}
