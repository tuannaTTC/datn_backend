package com.example.datn.repositories;

import com.example.datn.entity.UserRate;
import com.example.datn.repositories.result.AdminQueryUseRateResult;
import com.example.datn.repositories.result.UserRateResult;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRateRepository extends JpaRepository<UserRate, Integer> {

    @Query(nativeQuery = true,
            value = "SELECT user_rate.id, " +
                    "       content, " +
                    "       user_rate.created, " +
                    "       user_rate.total_like, " +
                    "       user_rate.total_dislike, " +
                    "       user_rate.point_rate, " +
                    "       user_rate.title, " +
                    " " +
                    "       users.avatar, " +
                    "       users.fullname " +
                    "FROM user_rate " +
                    "         LEFT JOIN users " +
                    "                   ON user_rate.user_id = users.id " +
                    "WHERE user_rate.movie_id = ?1")
    List<UserRateResult> findAllByMovieId(Integer movieId);

    @Query(nativeQuery = true,
            value = "SELECT user_rate.id, " +
                    "    user_rate.user_id, " +
                    "    user_rate.created, " +
                    "    user_rate.content, " +
                    "    users.fullname, " +
                    "    user_rate.total_like, " +
                    "    user_rate.total_dislike, " +
                    "    user_rate.point_rate, " +
                    "    movies.name " +
                    "    FROM user_rate " +
                    "    LEFT JOIN users " +
                    "    ON user_rate.user_id = users.id " +
                    "    LEFT JOIN movies " +
                    "    ON user_rate.movie_id = movies.id " +
                    "    WHERE IF(?1 IS NOT NULL, user_rate.user_id = ?1, TRUE) ",
            countQuery = "SELECT COUNT(user_rate.id) " +
                    "FROM user_rate " +
                    "         LEFT JOIN users " +
                    "                   ON user_rate.user_id = users.id " +
                    "         LEFT JOIN movies " +
                    "                   ON user_rate.movie_id = movies.id " +
                    "WHERE IF(?1 IS NOT NULL, user_rate.user_id = ?1, TRUE)")
    Page<AdminQueryUseRateResult> adminFindAllByUserId(Integer userId, Pageable pageable);


}
