package com.example.datn.repositories;

import com.example.datn.entity.UserComment;
import com.example.datn.repositories.result.AdminQueryUserCommentResult;
import com.example.datn.repositories.result.UserCommentResult;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserCommentRepository extends JpaRepository<UserComment, Integer> {

    @Query(nativeQuery = true,
            value = "SELECT user_comment.id, " +
                    "       content, " +
                    "       user_comment.created, " +
                    "       user_comment.total_like, " +
                    "       user_comment.total_dislike, " +
                    " " +
                    "       users.avatar, " +
                    "       users.fullname " +
                    "FROM user_comment " +
                    "         LEFT JOIN users " +
                    "                   ON user_comment.user_id = users.id " +
                    "WHERE user_comment.movie_id = ?1")
    List<UserCommentResult> findAllByMovieId(Integer movieId);

    @Query(nativeQuery = true,
            value = "SELECT user_comment.id, " +
                    "       user_comment.user_id, " +
                    "       user_comment.created, " +
                    "       user_comment.content, " +
                    "       users.fullname, " +
                    "       user_comment.total_like, " +
                    "       user_comment.total_dislike, " +
                    "       movies.name " +
                    "FROM user_comment " +
                    "         LEFT JOIN users " +
                    "                   ON user_comment.user_id = users.id " +
                    "         LEFT JOIN movies " +
                    "                   ON user_comment.movie_id = movies.id " +
                    "WHERE IF(?1 IS NOT NULL, user_comment.user_id = ?1, TRUE) ",
            countQuery = "SELECT COUNT(user_comment.id) " +
                    "FROM user_comment " +
                    "         LEFT JOIN users " +
                    "                   ON user_comment.user_id = users.id " +
                    "         LEFT JOIN movies " +
                    "                   ON user_comment.movie_id = movies.id " +
                    "WHERE IF(?1 IS NOT NULL, user_comment.user_id = ?1, TRUE) ")
    Page<AdminQueryUserCommentResult> adminFindAllByUserId(Integer userId, Pageable pageable);

}
