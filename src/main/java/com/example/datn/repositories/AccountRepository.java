package com.example.datn.repositories;

import com.example.datn.entity.User;
import com.example.datn.repositories.result.AdminQueryUserResult;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<User, Integer> {

    User findById(int userId);

    User findByUsername(String username);

    boolean existsByUsername(String username);

    boolean existsByEmail(String email);

    @Query(nativeQuery = true,
            value = "SELECT users.id, " +
                    "       avatar, " +
                    "       username, " +
                    "       fullname, " +
                    "       email, " +
                    "       users.created, " +
                    "       user_comment.count_comment, " +
                    "       user_rate.count_rate " +
                    "FROM users " +
                    "LEFT JOIN " +
                    "  (SELECT user_comment.user_id, " +
                    "          COUNT(user_comment.id) AS count_comment " +
                    "   FROM user_comment " +
                    "   GROUP BY user_comment.user_id) AS user_comment ON users.id = user_comment.user_id " +
                    "LEFT JOIN " +
                    "  (SELECT user_rate.user_id, " +
                    "          COUNT(user_rate.id) AS count_rate " +
                    "   FROM user_rate " +
                    "   GROUP BY user_rate.user_id) AS user_rate ON users.id = user_rate.user_id " +
                    "GROUP BY users.id",
            countQuery = "COUNT(users.id) " +
                    "FROM users " +
                    "LEFT JOIN " +
                    "  (SELECT user_comment.user_id, " +
                    "          COUNT(user_comment.id) AS count_comment " +
                    "   FROM user_comment " +
                    "   GROUP BY user_comment.user_id) AS user_comment ON users.id = user_comment.user_id " +
                    "LEFT JOIN " +
                    "  (SELECT user_rate.user_id, " +
                    "          COUNT(user_rate.id) AS count_rate " +
                    "   FROM user_rate " +
                    "   GROUP BY user_rate.user_id) AS user_rate ON users.id = user_rate.user_id " +
                    "GROUP BY users.id")
    Page<AdminQueryUserResult> adminFindAll(Pageable pageable);

}
