package com.example.datn.repositories;

import com.example.datn.entity.UserWatch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserWatchRepository extends JpaRepository<UserWatch, Integer> {

    UserWatch findByUserIdAndMovieId(Integer userId, Integer movieId);

}
