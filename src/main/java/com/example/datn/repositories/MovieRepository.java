package com.example.datn.repositories;

import com.example.datn.entity.Movie;
import com.example.datn.repositories.result.AdminQueryAllFilm;
import com.example.datn.repositories.result.MovieCategoryResult;
import com.example.datn.repositories.result.TopPointMovieResult;
import com.example.datn.repositories.result.TopWatchMovieResult;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer> {

    Movie findByName(String name);

    @Query(nativeQuery = true,
            value = "SELECT movies.id, " +
                    "movies.image_link, " +
                    "movies.cast, " +
                    "movies.name " +
                    "FROM movies " +
                    "LEFT JOIN movie_category " +
                    "ON movies.id = movie_category.movie_id " +
                    "WHERE movie_category.category_id = ?1")
    Page<MovieCategoryResult> findByCategoryId(Integer categoryId, Pageable pageable);

    @Query(nativeQuery = true,
            value = "SELECT movies.id AS id, " +
                    "movies.name, " +
                    "movies.image_link, " +
                    "user_watch.user_id, " +
                    "movies.cast, " +
                    "COUNT(user_watch.user_id) AS view " +
                    "FROM user_watch " +
                    "LEFT JOIN movies " +
                    "ON movies.id = user_watch.movie_id " +
                    "GROUP BY movies.id " +
                    "ORDER BY view DESC " +
                    "LIMIT 18 ")
    List<TopWatchMovieResult> findTopViewMovie();

    @Query(nativeQuery = true,
            value = "SELECT user_rate.movie_id, " +
                    "movies.image_link, " +
                    "movies.cast, " +
                    "movies.name, " +
                    "SUM(user_rate.point_rate)/COUNT(user_id) as mean_point " +
                    "FROM user_rate " +
                    "LEFT JOIN movies " +
                    "ON user_rate.movie_id = movies.id " +
                    "GROUP BY user_rate.movie_id " +
                    "ORDER BY mean_point DESC " +
                    "LIMIT 18 ")
    List<TopPointMovieResult> findTopRateMovie();

    @Query(nativeQuery = true,
            value = "SELECT movies.id AS id, " +
                    "movies.name, " +
                    "movies.image_link, " +
                    "user_watch.user_id, " +
                    "movies.cast, " +
                    "COUNT(user_watch.user_id) AS view " +
                    "FROM user_watch " +
                    "LEFT JOIN movies " +
                    "ON movies.id = user_watch.movie_id " +
                    "WHERE user_watch.created BETWEEN ?1 AND ?2 " +
                    "GROUP BY movies.id " +
                    "ORDER BY view DESC " +
                    "LIMIT 18 ")
    List<TopWatchMovieResult> findTopViewWeekMovie(LocalDate startDate, LocalDate endDate);

    @Query(nativeQuery = true,
            value = "SELECT user_rate.movie_id, " +
                    "movies.image_link, " +
                    "movies.cast, " +
                    "movies.name, " +
                    "SUM(user_rate.point_rate)/COUNT(user_id) as mean_point " +
                    "FROM user_rate " +
                    "LEFT JOIN movies " +
                    "ON user_rate.movie_id = movies.id " +
                    "GROUP BY user_rate.movie_id " +
                    "ORDER BY mean_point DESC " +
                    "LIMIT 18 ")
    List<TopPointMovieResult> findTopRateWeekMovie(LocalDate startDate, LocalDate endDate);

    Page<Movie> findAllByCountryLike(String countryName, Pageable pageable);

    Page<Movie> findAllByNameLike(String name, Pageable pageable);

    @Query(nativeQuery = true,
            value = "SELECT movies.id AS id, " +
                    "       movies.name, " +
                    "       movies.created, " +
                    "       user_comment.total_cmt, " +
                    "       user_rate.total_rate, " +
                    "       user_watch.total_watch, " +
                    "       user_rate.mean_point " +
                    "FROM movies " +
                    "         LEFT JOIN (SELECT user_comment.movie_id, " +
                    "                           COUNT(user_id) AS total_cmt " +
                    "                    FROM user_comment " +
                    "                    GROUP BY user_comment.movie_id) AS user_comment ON movies.id = user_comment.movie_id " +
                    "         LEFT JOIN (SELECT user_rate.movie_id, " +
                    "                           COUNT(user_rate.user_id)                               AS total_rate, " +
                    "                           (SUM(user_rate.point_rate) / COUNT(user_rate.user_id)) AS mean_point " +
                    "                    FROM user_rate " +
                    "                    GROUP BY user_rate.movie_id) AS user_rate ON movies.id = user_rate.movie_id " +
                    "         LEFT JOIN (SELECT user_watch.movie_id, " +
                    "                           COUNT(user_watch.user_id) AS total_watch " +
                    "                    FROM user_watch " +
                    "                    GROUP BY user_watch.movie_id) AS user_watch ON movies.id = user_watch.movie_id " +
                    "GROUP BY movies.id ",
    countQuery = "SELECT COUNT(movies.id)" +
            "FROM movies " +
            "         LEFT JOIN (SELECT user_comment.movie_id, " +
            "                           COUNT(user_id) AS total_cmt " +
            "                    FROM user_comment " +
            "                    GROUP BY user_comment.movie_id) AS user_comment ON movies.id = user_comment.movie_id " +
            "         LEFT JOIN (SELECT user_rate.movie_id, " +
            "                           COUNT(user_rate.user_id)                               AS total_rate, " +
            "                           (SUM(user_rate.point_rate) / COUNT(user_rate.user_id)) AS mean_point " +
            "                    FROM user_rate " +
            "                    GROUP BY user_rate.movie_id) AS user_rate ON movies.id = user_rate.movie_id " +
            "         LEFT JOIN (SELECT user_watch.movie_id, " +
            "                           COUNT(user_watch.user_id) AS total_watch " +
            "                    FROM user_watch " +
            "                    GROUP BY user_watch.movie_id) AS user_watch ON movies.id = user_watch.movie_id " +
            "GROUP BY movies.id ")
    Page<AdminQueryAllFilm> findAllByManager(Pageable pageable);
}
