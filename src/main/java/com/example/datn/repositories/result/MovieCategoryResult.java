package com.example.datn.repositories.result;

import com.fasterxml.jackson.annotation.JsonProperty;

public interface MovieCategoryResult {

    Integer getId();

    String getName();

    @JsonProperty(value = "imageLink")
    String getImage_link();

    String getCast();

}
