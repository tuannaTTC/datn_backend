package com.example.datn.repositories.result;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public interface AdminQueryUserResult {
    Integer getId();
    String getAvatar();
    String getUsername();
    String getFullname();
    String getEmail();
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    Date getCreated();

    Integer getCount_comment();
    Integer getCount_rate();
}
