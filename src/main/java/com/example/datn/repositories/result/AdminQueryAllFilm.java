package com.example.datn.repositories.result;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public interface AdminQueryAllFilm {
    Integer getId();

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    Date getCreated();

    String getName();

    Integer getTotal_cmt();

    Integer getTotal_rate();

    Integer getTotal_watch();

    Double getMean_point();

}
