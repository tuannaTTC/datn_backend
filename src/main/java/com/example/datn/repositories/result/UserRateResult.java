package com.example.datn.repositories.result;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public interface UserRateResult {
    Integer getId();

    String getContent();

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "GMT+07:00")
    Date getCreated();

    Integer getTotal_like();

    Integer getTotal_dislike();

    String getAvatar();

    String getFullname();

    Double getPoint_rate();

    String getTitle();
}
