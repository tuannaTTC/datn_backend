package com.example.datn.repositories.result;

public interface TopWatchMovieResult {

    Integer getId();

    Integer getUser_id();

    Integer getView();

    String getName();

    String getImage_link();

    String getCast();
}
