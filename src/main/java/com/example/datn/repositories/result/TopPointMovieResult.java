package com.example.datn.repositories.result;

public interface TopPointMovieResult {

    Integer getMovie_id();

    Double getMean_point();

    String getImage_link();

    String getCast();

    String getName();

}
