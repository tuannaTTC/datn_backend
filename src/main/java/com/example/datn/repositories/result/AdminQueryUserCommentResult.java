package com.example.datn.repositories.result;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public interface AdminQueryUserCommentResult {

    Integer getId();

    Integer getUser_id();

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    Date getCreated();

    String getContent();

    String getFullname();

    Integer getTotal_like();

    Integer getTotal_dislike();

    String getName();

}
