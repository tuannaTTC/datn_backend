package com.example.datn.services;

import com.example.datn.entity.UserComment;
import com.example.datn.interceptors.AuthInterceptor;
import com.example.datn.models.request.CreateUserCommentRequest;
import com.example.datn.models.response.BaseResponse;
import com.example.datn.repositories.UserCommentRepository;
import com.example.datn.utils.Global;
import com.example.datn.utils.StatusResponse;
import com.example.datn.utils.StringResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class UserCommentService {

    @Autowired
    private AuthInterceptor interceptor;

    @Autowired
    private UserCommentRepository repository;

    public ResponseEntity<?> getByMovieId(Integer movieId) {
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, repository.findAllByMovieId(movieId)));
    }

    public ResponseEntity<?> create(HttpServletRequest request, CreateUserCommentRequest userRequest) {
        UserComment userComment = new UserComment();

        BeanUtils.copyProperties(userRequest, userComment);
        userComment.setUserId(interceptor.getAccountID(request));
        userComment.setTotalLike(Global.TOTAL_LIKE_DEFAULT);
        userComment.setTotalDislike(Global.TOTAL_DISLIKE_DEFAULT);

        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, repository.save(userComment)));
    }

    public ResponseEntity<?> adminGetAllCommentByUserId(Integer userId, Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page, size);
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, repository.adminFindAllByUserId(userId, pageable)));
    }

    public ResponseEntity<?> adminGetAllComment(Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page, size);
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, repository.adminFindAllByUserId(null, pageable)));
    }

    public ResponseEntity<?> countAll() {
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, repository.count()));
    }

    public ResponseEntity<?> getById(Integer id) {
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, repository.findById(id)));
    }

    public ResponseEntity<?> like(Integer id) {
        UserComment userComment = repository.findById(id).orElse(null);

        if (userComment == null) {
            return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, "Update successful"));
        }

        userComment.setTotalLike(userComment.getTotalLike() + 1);
        repository.save(userComment);
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, "Like successful"));
    }
}
