package com.example.datn.services;

import com.example.datn.models.response.BaseResponse;
import com.example.datn.repositories.CategoryRepository;
import com.example.datn.utils.StatusResponse;
import com.example.datn.utils.StringResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository repository;

    public ResponseEntity<?> getList(){
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK,repository.findAll()));
    }

}
