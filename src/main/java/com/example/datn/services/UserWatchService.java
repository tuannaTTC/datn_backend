package com.example.datn.services;

import com.example.datn.models.response.BaseResponse;
import com.example.datn.repositories.UserWatchRepository;
import com.example.datn.utils.StatusResponse;
import com.example.datn.utils.StringResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserWatchService {
    @Autowired
    private UserWatchRepository repository;

    public ResponseEntity<?> countAll() {
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK,repository.count()));
    }
}
