package com.example.datn.services;

import com.example.datn.entity.UserRate;
import com.example.datn.interceptors.AuthInterceptor;
import com.example.datn.models.request.CreateRateRequest;
import com.example.datn.models.response.BaseResponse;
import com.example.datn.repositories.UserRateRepository;
import com.example.datn.utils.Global;
import com.example.datn.utils.StatusResponse;
import com.example.datn.utils.StringResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@Service
public class UserRateService {

    @Autowired
    private UserRateRepository repository;

    @Autowired
    private AuthInterceptor interceptor;

    public ResponseEntity<?> getByMovieId(Integer movieId) {
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, repository.findAllByMovieId(movieId)));
    }

    public ResponseEntity<?> create(HttpServletRequest request, CreateRateRequest rateRequest) {
        UserRate userRate = new UserRate();

        BeanUtils.copyProperties(rateRequest, userRate);
        userRate.setUserId(interceptor.getAccountID(request));
        userRate.setTotalLike(Global.TOTAL_LIKE_DEFAULT);
        userRate.setTotalDislike(Global.TOTAL_DISLIKE_DEFAULT);

        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, repository.save(userRate)));
    }


    public ResponseEntity<?> adminGetAllRate(Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page, size);
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, repository.adminFindAllByUserId(null, pageable)));
    }

    public ResponseEntity<?> adminGetAllRateByUserId(Integer userId, Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page, size);
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, repository.adminFindAllByUserId(userId, pageable)));

    }

    public ResponseEntity<?> countAll() {
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, repository.count()));
    }

    public ResponseEntity<?> getById(Integer id) {
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, repository.findById(id)));
    }

    public ResponseEntity<?> like(Integer id) {
        UserRate userRate = repository.findById(id).orElse(null);

        if (userRate == null) {
            return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, "Update successful"));
        }

        userRate.setTotalLike(userRate.getTotalLike() + 1);
        repository.save(userRate);
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, "Like successful"));
    }
}
