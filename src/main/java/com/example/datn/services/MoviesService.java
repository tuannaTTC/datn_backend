package com.example.datn.services;

import com.example.datn.entity.*;
import com.example.datn.interceptors.AuthInterceptor;
import com.example.datn.models.request.CreateMovieRequest;
import com.example.datn.models.response.BaseResponse;
import com.example.datn.models.response.MovieResponse;
import com.example.datn.repositories.*;
import com.example.datn.utils.Global;
import com.example.datn.utils.StatusResponse;
import com.example.datn.utils.StringResponse;
import com.example.datn.utils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class MoviesService {

    @Autowired
    private MovieRepository repository;

    @Autowired
    private MovieCategoryRepository movieCategoryRepository;

    @Autowired
    private UserWatchRepository userWatchRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private AuthInterceptor interceptor;

    @Autowired
    private AccountRepository accountRepository;

    //Get
    public ResponseEntity<?> get(Integer id) {
        Movie movie = repository.findById(id).orElse(null);
        if (movie == null) {
            return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, null));
        }

        List<MovieCategory> list = movieCategoryRepository.findAllByMovieId(movie.getId());
        List<String> listCategoryName = list.stream()
                .map(it -> {
                    Category category = categoryRepository.findById(it.getCategoryId()).orElse(null);
                    if (category == null) {
                        return "";
                    }
                    return category.getName();
                })
                .collect(Collectors.toList());

        MovieResponse movieResponse = new MovieResponse(movie, listCategoryName);
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, movieResponse));
    }

    public ResponseEntity<?> getByCategory(Integer categoryId, Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page, size);
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, repository.findByCategoryId(categoryId, pageable)));
    }

    //Post
    public ResponseEntity<?> create(CreateMovieRequest movieRequest) {
        Movie movie = repository.findByName(movieRequest.getName());

        if (movie != null) {
            return ResponseEntity.ok(new BaseResponse<>(StatusResponse.BAD_REQUEST, "Film does exit", movie));
        }

        movie = new Movie();
        movie.setCast(movieRequest.getCast());
        movie.setName(movieRequest.getName());
        movie.setCountry(movieRequest.getCountry());
        movie.setDescription(movieRequest.getDescription());
        movie.setDirector(movieRequest.getDirector());
        movie.setMinutes(movieRequest.getMinutes());
        movie.setYear(movieRequest.getYear());

        Movie movieSave = repository.save(movie);

        List<MovieCategory> list = movieRequest.getCategories()
                .stream()
                .map(it -> {
                    MovieCategory movieCategory = new MovieCategory();

                    movieCategory.setCategoryId(it);
                    movieCategory.setMovieId(movieSave.getId());

                    return movieCategory;
                })
                .collect(Collectors.toList());

        movieCategoryRepository.saveAll(list);

        this.saveImage(movieRequest.getImage(), movieSave.getId());

        this.saveFilm(movieRequest.getMovie(), movieSave.getId());

        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, movie));
    }

    private void saveImage(MultipartFile image, Integer id) {
        String nameImage = image.getOriginalFilename();

        if (nameImage != null && nameImage.length() > 0) {
            try {
                File serverFile = new File("G:/Java/website_movie2/public/picture/" + id + ".jpg");
                //uploadRootDir.getAbsolutePath() + File.separator
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                stream.write(image.getBytes());
                stream.close();

                System.out.println("Write file: " + serverFile);
            } catch (Exception e) {
                System.out.println("Error Write file: " + nameImage);
            }
        }
    }

    private void saveFilm(MultipartFile film, Integer id) {
        String nameMovie = film.getOriginalFilename();

        if (nameMovie != null && nameMovie.length() > 0) {
            try {
                File serverFile = new File("G:/Java/website_movie2/public/videos/" + id + ".mp4");
                //uploadRootDir.getAbsolutePath() + File.separator
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                stream.write(film.getBytes());
                stream.close();

                System.out.println("Write file: " + serverFile);
            } catch (Exception e) {
                System.out.println("Error Write file: " + nameMovie);
            }
        }
    }

    public ResponseEntity<?> createUserMovie(HttpServletRequest request, Integer movieId) {

        UserWatch userMovie = userWatchRepository.findByUserIdAndMovieId(interceptor.getAccountID(request), movieId);

        if (userMovie != null) {
            return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, userMovie));
        }

        userMovie = new UserWatch();
        userMovie.setUserId(interceptor.getAccountID(request));
        userMovie.setMovieId(movieId);

        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, userWatchRepository.save(userMovie)));
    }

    public ResponseEntity<?> getNewMovie(Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("created").descending());
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, repository.findAll(pageable)));
    }

    public ResponseEntity<?> getTopView() {
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, repository.findTopViewMovie()));
    }

    public ResponseEntity<?> getTopRate() {
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, repository.findTopRateMovie()));
    }

    public ResponseEntity<?> getByName(String name, Integer page, Integer size) {
        name = StringUtil.buildKeyword(name);
        Pageable pageable = PageRequest.of(page, size);
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, repository.findAllByNameLike(name, pageable)));
    }

    public ResponseEntity<?> getByCountryName(String countryName, Integer page, Integer size) {
        countryName = StringUtil.buildKeyword(countryName);
        Pageable pageable = PageRequest.of(page, size);
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, repository.findAllByCountryLike(countryName, pageable)));
    }

    public ResponseEntity<?> getTopViewWeek() {
        LocalDate now = LocalDate.now();
        LocalDate startDate = now.plusWeeks(-1);
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, repository.findTopViewWeekMovie(startDate, now)));
    }

    public ResponseEntity<?> getTopRateWeek() {
        LocalDate now = LocalDate.now();
        LocalDate startDate = now.plusWeeks(-1);
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, repository.findTopRateWeekMovie(startDate, now)));
    }

    public ResponseEntity<?> countAll() {
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, repository.count()));
    }

    public ResponseEntity<?> managerByAdmin(HttpServletRequest request, Integer page, Integer size) {
        User user = accountRepository.findById(interceptor.getAccountID(request));

        if (Objects.isNull(user)) {
            return ResponseEntity.ok(new BaseResponse<>(StatusResponse.BAD_REQUEST, "User not found"));
        }

        if (Global.ADMIN != user.getRoleId()) {
            return ResponseEntity.ok(new BaseResponse<>(StatusResponse.BAD_REQUEST, "has not permission"));
        }

        Pageable pageable = PageRequest.of(page, size);
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, repository.findAllByManager(pageable)));
    }
}
