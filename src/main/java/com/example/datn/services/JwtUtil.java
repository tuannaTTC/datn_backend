package com.example.datn.services;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.example.datn.entity.User;
import com.example.datn.models.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.Instant;
import java.util.Date;

@Component
public class JwtUtil {

    private Algorithm algorithm;
    private JWTVerifier jwtVerifier;

    @Autowired
    public JwtUtil(@Value("${app.jwt.secret}") String secretJwt) {
        this.algorithm = Algorithm.HMAC256(secretJwt);
        this.jwtVerifier = JWT.require(this.algorithm).build();
    }

    public String generateJWT(Claims claims) {
        long durationLiveInMilli = 3600000;
        Date now = new Date();
        Date validity = new Date(now.getTime() + durationLiveInMilli);

        String token = JWT.create()
                .withIssuedAt(Date.from(Instant.now()))
                .withExpiresAt(validity)
                .withClaim("user_id", claims.getUserId())
                .withClaim("role_id", claims.getRoleId())
                .withClaim("username", claims.getUsername())
                .sign(this.algorithm);

        return token;
    }

    public DecodedJWT validateJWT(String token) {
        DecodedJWT decodedJWT;
        try {
            decodedJWT = this.jwtVerifier.verify(token);
        } catch (JWTVerificationException e) {
            return null;
        }
        return decodedJWT;
    }

    public String generateToken(User user) {

        String token = generateJWT(Claims.from(user));
        return token;
    }

    public int exportUserId(String token) {
        DecodedJWT decodedJWT;
        try {
            decodedJWT = this.jwtVerifier.verify(token);
        } catch (JWTVerificationException e) {
            return -1;
        }

        return Claims.from(decodedJWT).getUserId();
    }
}

