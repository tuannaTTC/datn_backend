package com.example.datn.services;

import com.example.datn.entity.User;
import com.example.datn.interceptors.AuthInterceptor;
import com.example.datn.models.request.ChangePassword;
import com.example.datn.models.request.LoginRequest;
import com.example.datn.models.request.RegisterRequest;
import com.example.datn.models.request.UpdateUserRequest;
import com.example.datn.models.response.BaseResponse;
import com.example.datn.models.response.LoginResponse;
import com.example.datn.repositories.AccountRepository;
import com.example.datn.utils.StatusResponse;
import com.example.datn.utils.StringResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;


@Service
public class AccountService {

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private AccountRepository repository;

    @Autowired
    private AuthInterceptor interceptor;

    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    public ResponseEntity<?> register(RegisterRequest request) {

        if (repository.existsByUsername(request.getUsername())) {
            return ResponseEntity.ok(new BaseResponse<>(StatusResponse.BAD_REQUEST, "Usename Đã tồn tại", request));
        }

        if (repository.existsByEmail(request.getEmail())) {
            return ResponseEntity.ok(new BaseResponse<>(StatusResponse.BAD_REQUEST, "Email Đã tồn tại", request));
        }

        if (!request.getPassword().equals(request.getRepeatPassword())) {
            return ResponseEntity.ok(new BaseResponse<>(StatusResponse.BAD_REQUEST, "Mật khẩu không trùng nhau", request));
        }

        User user = new User();
        user.setUsername(request.getUsername());
        user.setFullname(request.getFullname());
        user.setEmail(request.getEmail());
        user.setPassword(passwordEncoder.encode(request.getPassword()));

        repository.save(user);

        LoginResponse response = new LoginResponse();
        response.setUser(user);

        String token = jwtUtil.generateToken(user);
        response.setToken(token);

        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, response));
    }


    public ResponseEntity<?> login(LoginRequest request) {

        User user = repository.findByUsername(request.getUsername());

        if (user == null) {
            return ResponseEntity.ok(new BaseResponse<>(StatusResponse.BAD_REQUEST, "Tài khoản không tồn tại", request));
        }

        if (!passwordEncoder.matches(request.getPassword(), user.getPassword())) {
            return ResponseEntity.ok(new BaseResponse<>(StatusResponse.BAD_REQUEST, "Mật khẩu không đúng", request));
        }

        String token = jwtUtil.generateToken(user);
        LoginResponse response = new LoginResponse();
        response.setToken(token);
        response.setUser(user);

        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, response));
    }

    public ResponseEntity<?> update(HttpServletRequest request, UpdateUserRequest updateUserRequest) {
        User user = repository.findById(interceptor.getAccountID(request));

        if (user == null) {
            return ResponseEntity.ok(new BaseResponse<>(StatusResponse.BAD_REQUEST, "User not found", updateUserRequest));
        }

        BeanUtils.copyProperties(updateUserRequest, user);
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, repository.save(user)));
    }

    public ResponseEntity<?> changePassword(HttpServletRequest request, ChangePassword changePassword) {
        User user = repository.findById(interceptor.getAccountID(request));

        if (user == null) {
            return ResponseEntity.ok(new BaseResponse<>(StatusResponse.BAD_REQUEST, "User not found", null));
        }

        if (!passwordEncoder.matches(changePassword.getOldPassword(), user.getPassword())) {
            return ResponseEntity.ok(new BaseResponse<>(StatusResponse.BAD_REQUEST, "Password cũ không hợp lệ", changePassword));
        }

        if (!changePassword.getNewPassword().equals(changePassword.getConfirmPassword())) {
            return ResponseEntity.ok(new BaseResponse<>(StatusResponse.BAD_REQUEST, "Mật khẩu nhập vào không giống nhauu", changePassword));
        }

        user.setPassword(passwordEncoder.encode(changePassword.getNewPassword()));
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, repository.save(user)));
    }

    public ResponseEntity<?> adminQuery(Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page, size);
        return ResponseEntity.ok(new BaseResponse<>(StatusResponse.OK, StringResponse.OK, repository.adminFindAll(pageable)));
    }
}
