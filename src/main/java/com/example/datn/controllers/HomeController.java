package com.example.datn.controllers;

import com.example.datn.models.response.BaseResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Validated
@RequestMapping("/homes")
public class HomeController {

    @GetMapping()
    public ResponseEntity<?> read(){
        return ResponseEntity.ok(new BaseResponse<>(200,"0k"));
    }
}
