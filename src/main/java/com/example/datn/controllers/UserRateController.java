package com.example.datn.controllers;

import com.example.datn.models.request.CreateRateRequest;
import com.example.datn.services.UserRateService;
import com.example.datn.utils.PageDefault;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Validated
@RestController
@RequestMapping("/rate")
public class UserRateController {

    @Autowired
    private UserRateService service;

    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Integer id){
        return service.getById(id);
    }


    @GetMapping(value = "/movie")
    public ResponseEntity<?> getByMovieId(@RequestParam(name = "movie_id") Integer movieId) {
        return service.getByMovieId(movieId);
    }

    @GetMapping(value = "/admin")
    public ResponseEntity<?> adminGetAllRate(@RequestParam(defaultValue = PageDefault.PAGE) Integer page,
                                             @RequestParam(defaultValue = PageDefault.SIZE) Integer size) {
        return service.adminGetAllRate(page,size);
    }

    @GetMapping(value = "/admin/{userId}")
    public ResponseEntity<?> adminGetAllRateByUserId(@PathVariable("userId") Integer userId,
                                                     @RequestParam(defaultValue = PageDefault.PAGE) Integer page,
                                                     @RequestParam(defaultValue = PageDefault.SIZE) Integer size){
        return service.adminGetAllRateByUserId(userId,page,size);
    }

    @PostMapping
    public ResponseEntity<?> create(HttpServletRequest request, @Valid @RequestBody CreateRateRequest rateRequest) {
        return service.create(request, rateRequest);
    }

    @PostMapping(value = "/{id}")
    public ResponseEntity<?> create(@PathVariable("id") Integer id) {
        return service.like(id);
    }

    @GetMapping("/count")
    public ResponseEntity<?> countAll(){
        return service.countAll();
    }

}
