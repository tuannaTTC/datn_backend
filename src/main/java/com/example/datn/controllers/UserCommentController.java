package com.example.datn.controllers;


import com.example.datn.models.request.CreateUserCommentRequest;
import com.example.datn.services.UserCommentService;
import com.example.datn.utils.PageDefault;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@Validated
@RequestMapping("/comment")
public class UserCommentController {

    @Autowired
    private UserCommentService service;

    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Integer id){
        return service.getById(id);
    }

    @GetMapping(value = "/movie")
    public ResponseEntity<?> getByMovieId(@RequestParam(value = "movie_id") Integer movieId) {
        return service.getByMovieId(movieId);
    }

    @PostMapping()
    public ResponseEntity<?> create(HttpServletRequest request, @Valid @RequestBody CreateUserCommentRequest userRequest) {
        return service.create(request, userRequest);
    }


    @PostMapping(value = "/{id}")
    public ResponseEntity<?> create(@PathVariable("id") Integer id) {
        return service.like(id);
    }

    @GetMapping(value = "/admin/{userId}")
    public ResponseEntity<?> adminGetAllCommentByUserId(@PathVariable("userId") Integer userId,
                                                        @RequestParam(defaultValue = PageDefault.PAGE) Integer page,
                                                        @RequestParam(defaultValue = PageDefault.SIZE) Integer size) {
        return service.adminGetAllCommentByUserId(userId, page, size);
    }

    @GetMapping(value = "/admin")
    public ResponseEntity<?> adminGetAllComment(@RequestParam(defaultValue = PageDefault.PAGE) Integer page,
                                                @RequestParam(defaultValue = PageDefault.SIZE) Integer size) {
        return service.adminGetAllComment(page, size);
    }

    @GetMapping("/count")
    public ResponseEntity<?> countAll() {
        return service.countAll();
    }

}
