package com.example.datn.controllers;


import com.example.datn.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    public CategoryService service;

    @GetMapping(value = "/list")
    public ResponseEntity<?> getList() {
        return service.getList();
    }
}
