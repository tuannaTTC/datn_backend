package com.example.datn.controllers;

import com.example.datn.models.request.CreateMovieRequest;
import com.example.datn.services.MoviesService;
import com.example.datn.utils.PageDefault;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@Validated
@RequestMapping("/movies")
public class MovieController {

    @Autowired
    private MoviesService service;

    @GetMapping()
    public ResponseEntity<?> get(@RequestParam(name = "id") Integer id) {
        return service.get(id);
    }

    @GetMapping(value = "/category")
    public ResponseEntity<?> getByCategory(@RequestParam(name = "category_id") Integer categoryId,
                                           @RequestParam(defaultValue = PageDefault.PAGE) Integer page,
                                           @RequestParam(defaultValue = PageDefault.SIZE) Integer size) {
        return service.getByCategory(categoryId, page, size);
    }

    @GetMapping(value = "/new")
    public ResponseEntity<?> getNewMovie(@RequestParam(defaultValue = PageDefault.PAGE) Integer page,
                                         @RequestParam(defaultValue = PageDefault.SIZE) Integer size) {
        return service.getNewMovie(page, size);
    }

    @GetMapping(value = "/top-view")
    public ResponseEntity<?> getTopView() {
        return service.getTopView();
    }

    @GetMapping(value = "/top-rate")
    public ResponseEntity<?> getTopRate() {
        return service.getTopRate();
    }

    @GetMapping(value = "/list")
    public ResponseEntity<?> getByName(@RequestParam(required = false) String name,
                                       @RequestParam(defaultValue = PageDefault.PAGE) Integer page,
                                       @RequestParam(defaultValue = PageDefault.SIZE) Integer size) {
        return service.getByName(name, page, size);
    }

    @GetMapping(value = "/country/{countryName}")
    public ResponseEntity<?> getByCountry(@PathVariable("countryName") String countryName,
                                          @RequestParam(defaultValue = PageDefault.PAGE) Integer page,
                                          @RequestParam(defaultValue = PageDefault.SIZE) Integer size) {
        return service.getByCountryName(countryName, page, size);
    }

    @GetMapping(value = "/top-view-week")
    public ResponseEntity<?> getTopViewWeek() {
        return service.getTopViewWeek();
    }

    @GetMapping(value = "/top-rate-week")
    public ResponseEntity<?> getTopRateWeek() {
        return service.getTopRateWeek();
    }

    @PostMapping()
    public ResponseEntity<?> create(@RequestParam(name = "movie") MultipartFile movie,
                                    @RequestParam(name = "image") MultipartFile image,
                                    @RequestParam(name = "name") String name,
                                    @RequestParam(name = "description") String description,
                                    @RequestParam(name = "cast") String cast,
                                    @RequestParam(name = "director") String director,
                                    @RequestParam(name = "year") int year,
                                    @RequestParam(name = "minutes") int minutes,
                                    @RequestParam(name = "country") String country,
                                    @RequestParam(name = "category") List<Integer> category) {

        CreateMovieRequest request = new CreateMovieRequest(movie, image, name, description, year, minutes, country, director, cast, category);
        return service.create(request);
    }

    @PostMapping(value = "/watch")
    public ResponseEntity<?> createUserMovie(HttpServletRequest request, @RequestParam(name = "movie_id") Integer movieId) {
        return service.createUserMovie(request, movieId);
    }

    @GetMapping("/count")
    public ResponseEntity<?> countAll() {
        return service.countAll();
    }

    @GetMapping(value = "/admin")
    public ResponseEntity<?> managerByAdmin(HttpServletRequest request,
                                            @RequestParam(defaultValue = PageDefault.PAGE) Integer page,
                                            @RequestParam(defaultValue = PageDefault.SIZE) Integer size) {
        return service.managerByAdmin(request, page, size);
    }
}
