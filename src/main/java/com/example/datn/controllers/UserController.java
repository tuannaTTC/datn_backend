package com.example.datn.controllers;

import com.example.datn.models.request.ChangePassword;
import com.example.datn.models.request.LoginRequest;
import com.example.datn.models.request.RegisterRequest;
import com.example.datn.models.request.UpdateUserRequest;
import com.example.datn.services.AccountService;
import com.example.datn.utils.PageDefault;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/users")
@Validated
public class UserController {

    @Autowired
    private AccountService service;

    @PostMapping("/register")
    public ResponseEntity<?> register(@Valid @RequestBody RegisterRequest request) {
        return service.register(request);
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@Valid @RequestBody LoginRequest request) {
        return service.login(request);
    }

    @PutMapping("/update")
    public ResponseEntity<?> update(HttpServletRequest request, @Valid @RequestBody() UpdateUserRequest updateUserRequest){
        return service.update(request,updateUserRequest);
    }

    @PutMapping("/change-password")
    public ResponseEntity<?> changePassword(HttpServletRequest request,@Valid @RequestBody() ChangePassword changePassword){
        return service.changePassword(request,changePassword);
    }

    @GetMapping("/admin")
    public ResponseEntity<?> adminQuery(@RequestParam(defaultValue = PageDefault.PAGE) Integer page,
                                        @RequestParam(defaultValue = PageDefault.SIZE) Integer size){
        return service.adminQuery(page,size);
    }

}
