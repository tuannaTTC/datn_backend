package com.example.datn.controllers;


import com.example.datn.services.UserWatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequestMapping("/watches")
public class UserWatchController {

    @Autowired
    private UserWatchService service;

    @GetMapping("/count")
    public ResponseEntity<?> countAll(){
        return service.countAll();
    }


}
