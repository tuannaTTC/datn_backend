package com.example.datn.utils;

import org.springframework.util.StringUtils;

public class StringUtil {
    private StringUtil() {

    }

    public static String buildKeyword(String keyword) {
        if (!StringUtils.hasText(keyword)) {
            return "%";
        }
        return "%" + keyword.toLowerCase() + "%";
    }

}
