package com.example.datn.utils;

public interface Global {

    int ACTIVE = 1;
    int ADMIN = 1;
    int INACTIVE = 0;
    int TOTAL_LIKE_DEFAULT = 0;
    int TOTAL_DISLIKE_DEFAULT = 0;
    String USERNAME_PATTERN = "^[a-zA-Z0-9]{3,15}$";

}