package com.example.datn.utils;

public interface StringResponse {
    String UNAUTHORIZED = "UNAUTHORIZED!";
    String OK = "SUCCESS!";
    String INVALID_TOKEN = "invalid token!";
    String NOT_FOUND = "nopt found";

    //category
    String CATEGORY_NOT_FOUND = "category is not exist";
    String OPTION_CONDITION_ERROR = "Option create fields is not null";

    // profile
    String AGE_ENOUGH = "user is not enough age to get account";
    String IMAGES_ENOUGH = "images upload are not enough to update profile";
    String QUESTION_ENOUGH = "questions is not enough to update profile";
    String USER_ID_FAKE = "user id is fake";

    String INVALID_MULTIPART_FILES = "invalid multipart file !";
}