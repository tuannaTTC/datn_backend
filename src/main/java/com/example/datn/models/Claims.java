package com.example.datn.models;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.example.datn.entity.User;

public class Claims {
    private int userId;
    private int roleId;
    private String username;

    private DecodedJWT decodedJWT;

    public Claims(int userId, int roleId, String username) {
        this.userId = userId;
        this.roleId = roleId;
        this.username = username;
    }

    public static Claims from(DecodedJWT decodedJWT) {
        return new Claims(
                decodedJWT.getClaim("user_id").asInt(),
                decodedJWT.getClaim("role_id").asInt(),
                decodedJWT.getClaim("username").asString()
        );
    }

    public static Claims from(User user) {
        return new Claims(
                user.getId(),
                user.getRoleId(),
                user.getUsername()
        );
    }

    public int getUserId() {
        return this.userId;
    }

    public int getRoleId() {
        return this.roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void  setUsername(String username){
        this.username = username;
    }

    public String getUsername(){
        return this.username;
    }

    @Override
    public String toString() {
        return "Claims{" +
                "userId='" + userId + '\'' +
                "roleId='" + roleId + '\'' +
                ", decodedJWT=" + decodedJWT +
                '}';
    }
}
