package com.example.datn.models.response;

import com.example.datn.entity.Movie;

import java.util.List;

public class MovieResponse {

    private Movie movie;

    private List<String> category;

    public MovieResponse(Movie movie, List<String> category) {
        this.movie = movie;
        this.category = category;
    }

    public MovieResponse() {
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public List<String> getCategory() {
        return category;
    }

    public void setCategory(List<String> category) {
        this.category = category;
    }
}
