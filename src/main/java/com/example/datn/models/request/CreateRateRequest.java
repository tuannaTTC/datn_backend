package com.example.datn.models.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

public class CreateRateRequest {

    @JsonProperty("movie_id")
    @Min(0)
    private Integer movieId;

    @Range(min = 0, max = 10)
    @JsonProperty("point_rate")
    private Double pointRate;

    @NotBlank
    private String content;

    public CreateRateRequest() {
    }

    public Integer getMovieId() {
        return movieId;
    }

    public void setMovieId(Integer movieId) {
        this.movieId = movieId;
    }

    public Double getPointRate() {
        return pointRate;
    }

    public void setPointRate(Double pointRate) {
        this.pointRate = pointRate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
