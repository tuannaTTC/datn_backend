package com.example.datn.models.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

public class CreateUserCommentRequest {

    public CreateUserCommentRequest() {
    }

    @Min(0)
    @JsonProperty("movie_id")
    private Integer movieId;

    @NotBlank
    private String content;



    public Integer getMovieId() {
        return movieId;
    }

    public void setMovieId(Integer movieId) {
        this.movieId = movieId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
