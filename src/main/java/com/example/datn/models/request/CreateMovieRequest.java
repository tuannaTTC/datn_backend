package com.example.datn.models.request;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.MultipartFilter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

public class CreateMovieRequest {

    private MultipartFile movie;

    private MultipartFile image;

    @NotBlank
    private String name;

    @NotBlank
    private String description;

    @Min(0)
    private Integer year;

    @Min(0)
    private Integer minutes;

    @NotBlank
    private String country;

    @NotBlank
    private String director;

    @NotBlank
    private String cast;

    @NotNull
    private List<Integer> categories;

    public CreateMovieRequest() {

    }

    public CreateMovieRequest(MultipartFile movie, MultipartFile image, @NotBlank String name, @NotBlank String description, @Min(0) Integer year, @Min(0) Integer minutes, @NotBlank String country, @NotBlank String director, @NotBlank String cast,@NotNull List<Integer> categories) {
        this.movie = movie;
        this.image = image;
        this.name = name;
        this.description = description;
        this.year = year;
        this.minutes = minutes;
        this.country = country;
        this.director = director;
        this.cast = cast;
        this.categories = categories;
    }

    public MultipartFile getMovie() {
        return movie;
    }

    public void setMovie(MultipartFile movie) {
        this.movie = movie;
    }

    public MultipartFile getImage() {
        return image;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMinutes() {
        return minutes;
    }

    public void setMinutes(Integer minutes) {
        this.minutes = minutes;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getCast() {
        return cast;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }

    public List<Integer> getCategories() {
        return categories;
    }

    public void setCategories(List<Integer> categories) {
        this.categories = categories;
    }
}
